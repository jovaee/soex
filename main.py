import os
import re
import argparse
import subprocess

import eyed3
from eyed3 import mp3


video_formats = []


def add_meta_data(filename):
    """
    Adds meta data to the MP3 file, such as Artist, Title and Cover if possible
    :param filename: The filepath to the audio file
    :return: None
    """

    title = filename.replace('\\', '/').split('/')[-1].split('.')[0]
    artist = os.path.dirname(filename).replace('\\', '/').split('/')[-1].split('.')[0]
    cover = '.'.join(filename.split('.')[0:-1]) + '.jpg'
    
    audiofile = eyed3.load(filename)
    audiofile.initTag(eyed3.mp3.id3.ID3_V2_3)

    audiofile.tag.artist = unicode(artist)
    audiofile.tag.album_artist = unicode(artist)
    audiofile.tag.title = unicode(title)
    
    if os.path.isfile(cover):
        # Set front cover
        with open(cover, "rb") as image:
            audiofile.tag.images.set(3, image.read(), "image/jpeg")
    else:
        print '%s does not exist' % cover
        
    audiofile.tag.save()
    

def extract_sound(filename, bitrate=None, overwrite=False, log=False):
    """
    Extracts the audio from a video file and adds meta data to the MP3 file
    :param filename: The filepath to the video file
    :param bitrate: The bitrate the audio file should be saved in (not working)
    :param overwrite:
    :param log: Should logging be enabled (not working)
    :return: False if extraction failed, True if extraction was successful
    """
    
    FNULL = open(os.devnull, 'w')
    
    parts = filename.replace('\\', '/').split('/')
    # Remove any leading and trailing whitespaces from filename
    name = '.'.join(['.'.join(parts[-1].split('.')[0:-1]).strip(), parts[-1].split('.')[-1]])
    # Replace the old filename with the new clean one
    name = '/'.join(['/'.join(parts[0:-1]), name])
    # Changes file extensions
    mp3_file = ('.'.join(name.split('.')[0:-1]) + '.mp3')
    thumb_file = ('.'.join(name.split('.')[0:-1]) + '.jpg')
    
    # print mp3_file
    # print thumb_file
    
    try:
        if overwrite:
            # Generate a thumbnail from video at 15 seconds mark
            subprocess.call(args='ffmpeg -i "%s" -ss 15 -q:v 0 -vframes 1 -vf "scale=1280:720" -y "%s"' % (filename, thumb_file), stdout=FNULL, stderr=subprocess.STDOUT)
            
            subprocess.call(args='ffmpeg -i "%s" -y -vn -acodec mp3 -threads 4 "%s"' % (filename, mp3_file), stdout=FNULL, stderr=subprocess.STDOUT)
        else:
            # Generate a thumbnail from video at 15 seconds mark
            subprocess.call(args='ffmpeg -i "%s" -ss 15 -q:v 0 -vframes 1 -vf "scale=1280:720" -n "%s"' % (filename, thumb_file), stdout=FNULL, stderr=subprocess.STDOUT)
            
            subprocess.call(args='ffmpeg -i "%s" -n -vn -acodec mp3 -threads 4 "%s"' % (filename, mp3_file), stdout=FNULL, stderr=subprocess.STDOUT)

            add_meta_data(mp3_file)
            
    except Exception as e:
        print e
        print 'Something went wrong with "%s"' % mp3_file
        os.remove(thumb_file)  # Delete generated thumbnail
        return False
    
    os.remove(thumb_file)  # Delete generated thumbnail
    return True


def delete_audio_file(filename):
    """
    Deletes the MP3 file created from extracting audio
    :param filename: The filename of the video that was failed to extract audio from
    :return: None
    """

    file = '.'.join(filename.split('.')[0:-1]) + '.mp3'
    
    if os.path.isfile(file):
        os.remove(file)
        

def main(args):
    """
    
    :return: None
    """
    
    def is_video(filename):
        """
        Check if the file is 'n video file. The file's extension is compared.
        :param filename: The filepath to the file to check
        :return: True if a video file otherwise False
        """
        
        parts = filename.split('.')
        
        return parts[-1] in video_formats
    
    fails = []
    
    # Try to compile the regex if one was supplied
    if args.regex:
        try:
            pattern = re.compile(args.regex)
        except re.error:
            print 'Invalid regular expression supplied. Use "https://regex101.com/" to check regexs'
            exit()
            
    # Read the list of video formats
    load_video_formats()
    
    print args.directory
    
    if args.directory:
        # Go through all the directories supplied
        for dir in args.directory.split(','):
            dir = re.sub('"', '', dir)
            dir = re.sub("\\\\", '/', dir)
            
            for file in os.listdir(dir):
                # print dir + '/' + file
                
                if is_video(file):
                    print 'Processing %s' % file
                    
                    if args.regex and pattern.match(file):
                        if not extract_sound(dir + '/' + file, args.bitrate, args.overwrite, args.log):
                            fails.append(dir + '/' + file)
                            delete_audio_file(dir + '/' + file)
                    elif not args.regex:
                        if not extract_sound(dir + '/' + file, args.bitrate, args.overwrite, args.log):
                            fails.append(dir + '/' + file)
                            delete_audio_file(dir + '/' + file)
                    else:
                        print 'Skipping file "%s" as it does not match regex supplied' % file
    
    if args.file:
        # Go through all the individual files supplied
        for file in args.file:
            if is_video(file):
                print 'Processing %s' % file
                
                if args.regex and pattern.match(file):
                    if not extract_sound(file, args.bitrate, args.overwrite, args.log):
                        fails.append(file)
                        delete_audio_file(file)
                elif not args.regex:
                    if not extract_sound(file, args.bitrate, args.overwrite, args.log):
                        fails.append(file)
                        delete_audio_file(file)
                else:
                    print 'Skipping file "%s" as it does not match regex supplied' % file
    
    if len(fails) > 0:
        print 'Please redo the following files as they failed'
        print fails
        

def load_video_formats():
    """
    Read the JSON file that contains a list of all video formats
    :return: None
    """

    import json
    
    with open(os.path.dirname(os.path.abspath(__file__)).replace('\\', '/') + '/video-formats.json') as f:
        globals()['video_formats'] = json.load(f, encoding='utf-8')['formats']
    

def parse_args():
    """
    Parse the command-line arguments
    :return: The arguments
    """
    
    parser = argparse.ArgumentParser(description='Extracts audio from video files', add_help=True, version='1.1')
    
    parser.add_argument('--bitrate', '-b', default=None, type=str,
                        help='The bitrate the audio files should be saved in. Note that it is not guaranteed, only a best effort')
    parser.add_argument('--log', '-l', action='store_true', help='Enable logging')
    parser.add_argument('--regex', '-r', type=str,
                        help='Regular expression for specifying with files must be processed. Ex: Nightcore - [P-Z]{1}[a-zA-Z\s]+\.mp4')
    parser.add_argument('--overwrite', '-o', action='store_true', help='Overwrites MP3 if it already exists and re-extracts the audio')
    
    parser.add_argument('--directory', '-d', type=str,
                        help='Path to a directory of files that must be soex\'d. This argument can be supplied multiple times')
    parser.add_argument('--file', '-f', type=str,
                        help='Path to a file that must be soex\'d. This arguments can be supplied multiple times')
    
    args = parser.parse_args()
    
    print args
    
    return args


if __name__ == '__main__':
    args = parse_args()
    
    main(args)
